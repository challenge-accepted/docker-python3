FROM ubuntu:15.04

RUN apt-get update && apt-get install -y sudo language-pack-en vim net-tools git wget libgtkextra-dev libgconf-2-4 python3 python3-pip 

# IPython with Jupyter
RUN pip3 install jupyter

WORKDIR /home/user
ADD env.sh /home/user

RUN chmod u+x env.sh

# VSCode Install
#RUN wget https://go.microsoft.com/fwlink/?LinkID=760868 -O vcode.deb 
#RUN dpkg -i vcode.deb; exit 0
#RUN apt-get install -y -f

RUN useradd -ms /bin/bash user && echo "user:user" | chpasswd && adduser user sudo

CMD /bin/bash
