#!/bin/bash

OPTION=$1
EXTRA=$2
LOCALIP=`ifconfig | awk '/inet/{print substr($2, 0)}' | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | grep -vC 0 -oE "\b(127\.)([0-9]{1,3}\.){2}[0-9]{1,3}\b" | head -n +1`
NAME=python
LINKS=""
COMMAND="/bin/bash"
IMAGE="python-scipy"
PORTS="-p 8080:8080 -p 8888:8888"
VOLUMES="-v `pwd`/projects:/home/user/projects -v `pwd`/settings:/home/user/.vscode"
ENV="-e DISPLAY=$LOCALIP:0" # YOUR IP, BUT NOT LOCALHOST

if [ -z $OPTION ]; then
	OPTION=run
fi

if [ $OPTION = "run" ]; then
	docker run --rm -it $PORTS $ENV --name $NAME $VOLUMES $LINKS $IMAGE $COMMAND
fi

if [ $OPTION = "rm" ]; then
	docker rm -f $NAME
fi

if [ $OPTION = "stop" ]; then
	docker stop $NAME
fi

if [ $OPTION = "start" ]; then
	docker start $NAME
fi

if [ $OPTION = "attach" ]; then
	docker attach $NAME
fi
